import java.util.*;
import java.io.*;

public class InversionCountDriver {

   public static void main(String args[]) {
      InversionCount inv = new InversionCount();
      String filename;
      Scanner sc = new Scanner(System.in);
      ArrayList<Integer> A = new ArrayList<Integer>();
      FileInputStream fileIn;
      int n;

      try {
         System.out.print("Enter input file name: ");
         filename = sc.next();
         fileIn = new FileInputStream(new File(filename));
      }catch(FileNotFoundException e) {
         System.out.println("File not found: " + e.getMessage());
         return;
      }

      sc = new Scanner(fileIn);

      //Get number of ratings
      n = sc.nextInt();
      sc.nextLine();
      //Get ratings
      for (int i = 0; i < n; i++)
         A.add(sc.nextInt());

      System.out.println("The number of inversions is " + inv.countInversion(A));

      // System.out.print("Sorted array: ");
      // Iterator iter = A.listIterator();
      // while (iter.hasNext())
      //    System.out.print(iter.next() + " ");
      // System.out.println();

   }
}
