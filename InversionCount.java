import java.util.*;

public class InversionCount {

   public int countInversion(ArrayList<Integer> A) {
      ArrayList<Integer> B, C;
      int n = A.size(), count = 0;

      if (n > 1) {
         B = new ArrayList<Integer>(A.subList(0, n / 2));
         C = new ArrayList<Integer>(A.subList(n / 2, n));
         count += countInversion(B);
         count += countInversion(C);
         count += mergeAndCount(B, C, A);
      }

      return count;
   }

   private int mergeAndCount(
    ArrayList<Integer> B, ArrayList<Integer> C, ArrayList<Integer> A) {
      int i, j, k, count = 0;

      for (i = j = k = 0; i < B.size() && j < C.size(); k++) {
         if (B.get(i) < C.get(j))
            A.set(k, B.get(i++));
         else {
            count += B.size() - i;   //All elements > B[i] is inverted
            A.set(k, C.get(j++));
         }
      }
      if (i == B.size()) {
         while (j < C.size())
            A.set(k++, C.get(j++));
      }
      else {
         while (i < B.size())
            A.set(k++, B.get(i++));
      }
      return count;
   }

}
